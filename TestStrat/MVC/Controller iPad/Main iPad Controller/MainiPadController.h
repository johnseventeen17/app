//
//  MainiPadController.h
//  TestStrat
//
//  Created by John Lester Celis on 9/11/15.
//  Copyright (c) 2015 John Lester Celis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movies.h"

@interface MainiPadController : UITableViewController
@property (nonatomic, assign) id<MoviesSelectionDelegate> movieDelegate;
@end
