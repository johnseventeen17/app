//
//  Movies.h
//  TestStrat
//
//  Created by John Lester Celis on 9/11/15.
//  Copyright (c) 2015 John Lester Celis. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Movies;
@protocol MoviesSelectionDelegate <NSObject>

@required
- (void)selectedMovies:(Movies *)newMovies;
@end


@interface Movies : NSObject
{
    id <MoviesSelectionDelegate> _delegate;
}
@property (nonatomic, strong) id delegate;
@property (assign) id<MoviesSelectionDelegate> movieDelegate;
@property (strong, nonatomic) NSString *Title;
@property (strong, nonatomic) NSString *yearReleased;
@property (strong, nonatomic) NSString *Rating;
@property (strong, nonatomic) NSString *Overview;
@property (strong, nonatomic) NSString *imageSlug;
- (void)Sample;
+ (Movies *)moviesTitle:(NSString *)Title image:(NSString *)imageSlug yearReleased:(NSString *)yearReleased rating:(NSString *)rating;
- (void)selectedMovies:(Movies *)newMovies;
@end

