//
//  Movies.m
//  TestStrat
//
//  Created by John Lester Celis on 9/11/15.
//  Copyright (c) 2015 John Lester Celis. All rights reserved.
//

#import "Movies.h"

@implementation Movies
@synthesize movieDelegate;
@synthesize Title;
@synthesize imageSlug;
@synthesize yearReleased;
@synthesize Rating;

+ (Movies *)moviesTitle:(NSString *)Title image:(NSString *)imageSlug yearReleased:(NSString *)yearReleased rating:(NSString *)rating
{
    Movies *movie = [[Movies alloc] init];
    movie.Title = Title;
    movie.imageSlug = imageSlug;
    movie.yearReleased = yearReleased;
    movie.Rating = rating;
    
    return movie;
}

- (void)selectedMovies:(Movies *)newMovies
{
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self.delegate
                                   selector:@selector(ProcessCompleted) userInfo:nil repeats:NO];
}

@end
